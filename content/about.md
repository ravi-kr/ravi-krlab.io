---
title: "ABOUT ME"
date: 2020-12-13T18:06:02+05:30
draft: false
---

Hi, I'm Ravi. 
The following characteristics describes me
>1. Not a caffeine addict but a decent coder.
>2. Like building ~~cool stuff~~ cool stuff.
>3. I get things done.
>4. Like solving challenging problems.
>5. Haven't earned arrogance yet.

Besides these, I'm an Analyst and Developer living in Delhi. These days you'll find me exploring the field of Big Data Analytics and Business Intelligence (buzzwords, I do big data processing). I have previous experience in the field of Machine Learning, Deep Learning, and Cooking, equally.

If your work resembles anywhere with Engineering, Data Science, Biology, Psychology, Sociology, or anything about which you are passionate more than your date, feel free to drop me a message on any social media platform you like.

Au revoir!